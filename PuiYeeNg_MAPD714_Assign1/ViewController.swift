//
//  ViewController.swift
//  File Name: PuiYeeNg_MAPD714_Assign1
//  Author: Pui Yee Ng
//  StudentID: 301366105
//  Date: 9/18/2023
//  App description: Simple Interest Calculation App
//  Version: First Version

import UIKit

class ViewController: UIViewController {
    // Declare UI Components
    
    @IBOutlet weak var principalInput: UITextField!
    
    @IBOutlet weak var timeInput: UITextField!
    
    @IBOutlet weak var rateOfInterestInput: UITextField!
    
    @IBOutlet weak var interestResult: UILabel!
    
    @IBOutlet weak var totalAmountResult: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the colour of the result's Label's border
        interestResult.layer.borderColor = UIColor.purple.cgColor
        interestResult.layer.borderWidth = 3.0
        
        totalAmountResult.layer.borderColor = UIColor.purple.cgColor
        totalAmountResult.layer.borderWidth = 3.0
        
    }
    
    @IBAction func btnCalculateClicked(_ sender: Any) {
        // Get the value from text field
        
        let principalString = self.principalInput.text
        let timeString = self.timeInput.text
        let rateOfInterestString = self.rateOfInterestInput.text
        
        // Conversion the text field value to Float
        let principal = Double(principalString ?? "0") ?? 0
        let time = Double(timeString ?? "0") ?? 0
        let rateOfInterest = Double(rateOfInterestString ?? "0") ?? 0
            
        
        // Calculate Simple Interest and Total
        // SI = (P × R × T)/100
        // Total = P + SI
        
        let interest = principal * rateOfInterest *
        time / 100
        let total = principal + interest
        
        // Convert the interest and total to string
        let interestString = String (interest)
        let totalString = String (total)
        
        // Print the result of Interest and Total Amount
        interestResult.text = interestString
        totalAmountResult.text = totalString
        
    }
    
    // Clear Button that will be used to Reset the Text Fields to empty and Result Labels to Zero.
    @IBAction func btnClearClicked(_ sender: Any) {
        principalInput.text = ""
        timeInput.text = ""
        rateOfInterestInput.text = ""
        interestResult.text = "0"
        totalAmountResult.text = "0"
        
    }
    
}

